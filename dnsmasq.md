# Настройка DNSMasq для MacOS

## Отпадает необходимость в постоянной правке /etc/hosts файла

## Требования к системе

* [Homebrew](https://brew.sh/)

## Установка
```
brew install dnsmasq
```

## Настройка

### Создании директории для конфигурации
```
mkdir -pv $(brew --prefix)/etc/
```

### Настройка доменной зоны *.loc

```
echo 'address=/.loc/127.0.0.1' >> $(brew --prefix)/etc/dnsmasq.conf
```

## Запуск dnsmasq (будет автоматически запускаться после перезагрузки)
```
sudo brew services start dnsmasq
```

## Настройка resolvers

### Создадим директорию resolver
```
sudo mkdir -v /etc/resolver
```

### Добавим наше пространство имен в resolvers
```
sudo bash -c 'echo "nameserver 127.0.0.1" > /etc/resolver/loc'
```

### Просмотрим список всех resolvers, чтобы убедится что наш создался нормально
```
scutil --dns
```
