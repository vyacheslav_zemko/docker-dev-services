#!/usr/bin/env bash

# Переменные цветов, для более удобной работы с ними
RED='\033[0;31m'
WHITE='\033[1;37m'
GREEN='\033[0;32m'
ORANGE='\033[0;33m'
GREY='\033[0;37m'

clear

UNAMEOUT="$(uname -s)"

# Verify operating system is supported...
case "${UNAMEOUT}" in
  Linux*)             MACHINE="LINUX";;
  Darwin*)            MACHINE="MAC";;
  MINGW*)            MACHINE="WINDOWS";;
  *)                  MACHINE="UNKNOWN"
esac

if [ "$MACHINE" == "UNKNOWN" ]; then
  echo -e "${ORANGE}Unsupported operating system [$(uname -s)]. Supports macOS, Linux, and Windows (WSL2).${NC}" >&2

  exit 1
fi

if [ "$MACHINE" == "MAC" ]; then
  if ! command -v brew &> /dev/null
    then
      echo -e "${RED}Brew is not installed${NC}" >&2

      exit 1
  fi
fi

# Підключаємо .env файл
if [ -f "./.env" ]; then
    source "./.env"
else
    echo -e "${RED}Make sure you have defined the .env file${NC}\""

    exit 1
fi

DOMAINS=''

if [ "$2" == 'env' ]; then
  DOMAINS=$SSL_DOMAINS
else
  for domain in `cat /etc/hosts | tr " " "\n" | grep -o '[A-Za-z0-9_\.-]*.loc'`
   do
    DOMAINS="${DOMAINS} ${domain}"
  done
fi

if [ -z "$1" ]
  then
    echo 'Укажите директорию для хранения сертификатов'
    exit
fi

mkdir -p $1

echo $DOMAINS

WORKDIR=$PWD

if [ "$MACHINE" == "MAC" ]; then
  if ! command -v mkcert &> /dev/null
    then
        brew install mkcert
        brew install nss # if you use Firefox
  fi

  mkcert -uninstall || &> /dev/null
  mkcert -install \
    && mkcert -key-file $1/key.pem -cert-file $1/cert.pem localhost $DOMAINS

elif [ "$MACHINE" == "WINDOWS" ]; then
  mkcert -uninstall || &> /dev/null
  mkcert -install \
    && mkcert -key-file $1/key.pem -cert-file $1/cert.pem localhost $DOMAINS

else
  if command -v apt &> /dev/null
    then
      CERTUTIL_INSTALL='apt install libnss3-tools'
  elif command -v yum &> /dev/null
    then
      CERTUTIL_INSTALL='yum install nss-tools'
  elif command -v yum &> /dev/null
    then
      CERTUTIL_INSTALL='zypper install mozilla-nss-tools'
  else
    echo 'Unsupported OS'
    exit;
  fi

  if [ -d '/etc/pki/ca-trust/source/anchors/' ]; \
    then \
      SYSTEM_TRUST_COMMAND='update-ca-trust extract'; \

  elif [ -d '/usr/local/share/ca-certificates/' ]; \
    then \
      SYSTEM_TRUST_COMMAND='update-ca-certificates'; \

  elif [ -d '/etc/ca-certificates/trust-source/anchors/' ]; \
    then \
      SYSTEM_TRUST_COMMAND='trust extract-compat'; \

  elif [ -d '/usr/share/pki/trust/anchors' ]; \
    then \
      SYSTEM_TRUST_COMMAND='update-ca-certificates'; \

  else \
    echo 'Unsupported OS'; \
    exit;
  fi

  $CERTUTIL_INSTALL

  if ! command -v mkcert &> /dev/null
    then
      cd /usr/local/bin
      wget -O mkcert https://github.com/FiloSottile/mkcert/releases/download/v1.4.3/mkcert-v1.4.3-linux-amd64
      chmod +x ./mkcert
  fi

  mkcert -uninstall \
    && mkcert -install \
    && mkcert -key-file $1/key.pem -cert-file $1/cert.pem localhost $DOMAINS

  $SYSTEM_TRUST_COMMAND
fi

exit
