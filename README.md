1. Для начала нужно остановить контейнеры бекофиса + обратного прокси (который мы до этого использовали). Можно это сделать тут - https://i.imgur.com/ujnekU7.png
2. Сдед. шагом нужно прописать в /etc/hosts след записи (можно поменять на свои домены, не принципиально)
    - `127.0.0.1 lmcrm.loc` - адрес самой системы 
    - `127.0.0.1 white-label.loc` - адрес лейбла 
    - `127.0.0.1 pma.lmcrm.loc` - адрес phpmyadmin
    - `127.0.0.1 redis.lmcrm.loc` - адрес phpredisadmin
    - `127.0.0.1 traefik.loc` - адрес дашборда traefik
    - `127.0.0.1 mailhog.loc` - адрес дашборда SMTP сервера. Для разработки удобней будет его использовать + не будем спамить на основной SMTP сервер.
3. Перейти в папку, куда склонировали репозиторий `dev-services`
    - Создать необходимые сети с помощью команд: <br>
      `docker network create -d bridge frontend` <br>
      `docker network create -d bridge backend`
    - Настроить по своему усмотрению `.env` файл
    - Выполнить команду `make generate-certificates`. Она сгенерирует сертификаты, которые браузер будет принимать. Так же добавит их в список доверенных в системе. После выполнения этой команды нужно будет перезапустить браузеры.
    - Выполнить команду `make build`
    - Выполнить команду `make up`
    - Теперь уже можно перейти на `traefik.loc` и `mailhog.loc`
4. Для настройки непосредственно самого проекта необходимо перейти на ветку `docker-for-dev`
    - Отредактировать наш `.env`:
      - `DB_CONNECTION=mysql` <br>
        `DB_HOST=mysql` название докер контейнера с БД (не менять) <br>
        `DB_PORT=3306` порт к БД (не менять) <br>
        `DB_DATABASE=lmcrm` можно указать свое значение (должно совпадать с .env самого докера) <br>
        `DB_USERNAME=lmcrm` можно указать свое значение (должно совпадать с .env самого докера) <br>
        `DB_PASSWORD=lmcrm` можно указать свое значение (должно совпадать с .env самого докера)
      - Переключаем очереди на redis <br>
        `QUEUE_DRIVER=redis` <br>
        `QUEUE_REDIS_DATABASE=0` <br>
        `REDIS_HOST=redis` <br>
        `REDIS_PASSWORD=null` <br>
        `REDIS_PORT=6379` <br>
        `REDIS_DATABASE=0`
      - Меняем боевой smtp сервер для отправки почты на наш локальный (mailhog.loc) <br>
        `MAIL_DRIVER=smtp` <br>
        `MAIL_HOST=host.docker.internal` <br>
        `MAIL_PORT=1025` <br>
        `MAIL_USERNAME=null` <br>
        `MAIL_PASSWORD=null` <br>
        `MAIL_ENCRYPTION=null`
    - Перейти в папку `dev` в корне проекта
      - Удалить существующую папку `data` вместе со всем содержимым
      - Настроить по своему усмотрению `.env` файл (доступы к БД и т.д.)
      - Выполнить команду `make build`
      - Выполнить команду `make up`
      - Выполнить команду `make restore-mysql-dump [абсолютный путь к файлу дампа]`
      - Дальше нам уже будут доступны след. адреса: <br>
         `lmcrm.loc` <br>
         `pma.lmcrm.loc` <br>
         `redis.lmcrm.loc`
        
Для просмотра отправляемых мейлов нужно будет переходить по адресу `mailhog.loc`. Вот так это будет выглядеть - https://i.imgur.com/zNxIgIW.png

Что в `dev-services`, что в самом проекте (в папке `dev`) есть команда `make help`, которая выводит справку по всем доступным командам
